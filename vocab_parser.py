import xml.etree.ElementTree as ET
from alglornrithm import models
from flask_mongoengine import mongoengine

try:
    print('connect to database')
    mongoengine.connect('alglornrithmdb')

    with open('vocab.xml', encoding='iso8859_11') as xml_file:
        tree = ET.parse(xml_file)
        root = tree.getroot()

    print('parsing vocabs to db ....')
    for child in root.iter('tentry'):
        print('saving ', child.text, '...')
        vocab = models.Vocab.objects(content=child.text)
        if not vocab:
            vocab = models.Vocab(content=child.text)
            vocab.save()
            print(vocab.content, 'was save')
        else: print('duplicate vocab')
    print('parsing succeed')

except:
    models.Vocab.drop_collection()
    print('connection error')
