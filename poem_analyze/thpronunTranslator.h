#ifndef THPRONUNTRANSLATOR_H
#define THPRONUNTRANSLATOR_H
typedef struct {
    int score = 0;
    bool perfect = true;
    std::vector<std::string> pronounce;
    std::vector<int> interRhyme;
    std::vector<int> intraRhyme;
    std::vector<int> tone;
    std::vector<int> alphabetRhyme;
    std::vector<int> interChapterRhyme;
}Result;
const char *translateDataToCommand(std::string datas,std::string type);
std::vector<std::vector<std::string>> thpronunTextToSectionSyllable(const char *cmd);
std::vector<std::string> sectionToSyllable(std::string wuk);
Result callAnalyze(std::vector<std::string> buffer);
#endif