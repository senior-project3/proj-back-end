#ifndef THPRONUNTRANSLATOR_H
#define THPRONUNTRANSLATOR_H
const char *translateDataToCommand(string datas, string type);
vector<vector<string>> thpronunTextToSectionSyllable(const char *cmd);
vector<string> sectionToSyllable(string wuk);
Result poemAnalyze(vector<string> buffer);
#endif