#include <Python.h>
#include <bytesobject.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <vector>
#include <sstream>
#include <cstring>
#include <cstdlib>


using namespace std;

const char vowels[] = {
    'a','e','i','o','u'
};
#define VOWELS_COUNT 5

typedef struct {
    int score = 0;
    bool perfect = true;
    vector<string> pronounce;
    vector<int> interRhyme;
    vector<int> intraRhyme;
    vector<int> tone;
    vector<int> alphabetRhyme;
    vector<int> interChapterRhyme;
}Result;

const char *translateDataToCommand(string datas, string type) {
    string command = "thpronun -" + type + " " + datas;

    return command.c_str();
}

vector<vector<string>> thpronunTextToSectionSyllable(const char *cmd) {
    int count=0;
    array<char, 128> buffer;
    vector<string> sections;
    vector<vector<string>> result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        // if(count>0)
        sections.push_back( buffer.data() );
        // count++;
    }
    for(auto syllable : sections)
        result.push_back(sectionToSyllable(syllable));
    return result;
}

vector<string> sectionToSyllable(string wuk){
    vector<string> payang;
    istringstream iss(wuk);
    string temp = "";

    while(iss >> temp){
        payang.push_back(temp);
    }
    return payang;
}

bool checkRhyme(string sylA, string sylB){
    if(sylA == "" || sylB == ""){
        return false;
    }
    size_t sizeA = sylA.size()-1;
    size_t sizeB = sylB.size()-1;

    for(int i=0; i<VOWELS_COUNT; i++){
        if(sizeA > sylA.find(vowels[i]))
            sizeA = sylA.find(vowels[i]);
        if(sizeB > sylB.find(vowels[i]))
            sizeB = sylB.find(vowels[i]);
    }

    string vowelA = sylA.substr(sizeA, sylB.size()-(sizeA+1));
    string vowelB = sylB.substr(sizeB, sylB.size()-(sizeA+1));

    return (vowelA.compare(vowelB) == 0)? true : false;
}

bool checkAlphabet(string sylA, string sylB){
    if(sylA == "" || sylB == ""){
        return false;
    }
    
    size_t sizeA = sylA.size()-1;
    size_t sizeB = sylB.size()-1;

    for(int i=0; i<VOWELS_COUNT; i++){//size of vowels in thpronuns
        if(sizeA > sylA.find(vowels[i]))
            sizeA = sylA.find(vowels[i]);
        if(sizeB > sylB.find(vowels[i]))
            sizeB = sylB.find(vowels[i]);
    }
    string alphabetA = sylA.substr(0, sizeA);
    string alphabetB = sylB.substr(0, sizeB);

    return (alphabetA.compare(alphabetB) == 0)? true : false;
}

int checkInterSectionRhyme(vector<string> sectionA, vector<string> sectionB, int type){
    int result = 0;

    if(sectionA.size() == 0 || sectionB.size() == 0) {
        return 0;
    } else {
        switch (type)
        {
        case 1:
        case 3:
            if(checkRhyme(sectionA.at(sectionA.size()-1), sectionB.at(2)) ||
               checkRhyme(sectionA.at(sectionA.size()-1), sectionB.at(2)))
               result = 1;
            break;
        case 2:
        case 4:
            if(checkRhyme(sectionA.at(sectionA.size()-1), sectionB.at(sectionB.size()-1)))
               result = 1;
            break;
        default:
            break;
        }
    }
}

int checkIntraSectionRhyme(vector<string> section){
    int result = 0;

    if(section.size() == 0){
        return 0;
    }
    if(section.size() > 4){
        if(checkRhyme(section.at(2), section.at(3))){
            result+=1;
        }
    }
    if(section.size() > 7){
        if(checkRhyme(section.at(4), section.at(5)) || checkRhyme(section.at(4), section.at(6))){
            result+=1;
        }
    }
    
    return result;
}

int checkAlphabetRhyme(vector<string> section){
    int result=0;
    
    if(section.size() == 0){
        return 0;
    }

    for(int i=0; i<section.size()-1; i++){

        int j=i+1;
        while(j<section.size()  && checkAlphabet(section.at(i), section.at(j))){
            result++;
            j++;
        }
        i=j-1;
    }
    return result;
}

int checkTone(vector<string> section, int type){
    if(section.size() == 0){
        return 0;
    }

    string lastSyllabel = section.at(section.size()-1);
    char tone = lastSyllabel.at(lastSyllabel.size()-1);
    int result = 0;
    // 0 means bad, 1 means ok, 2 means good
    
    switch(type){
        case 1: if(tone == '0')
                    result = 1;
                else 
                    result = 2;
                break;
        case 2: if(tone == '0' || tone == '3')
                    result = 0;
                else if(tone == '4')
                    result = 2;
                else 
                    result = 1;
                break;
        case 3: // same as case 4
        case 4: if(tone == '0' || tone == '3')
                    result = 2;
                else 
                    result = 0;
                break;
        default: break;
    }

    return result;
}

vector<vector<vector<string>>> translateDataToPoem(vector<string> data){
    vector<vector<vector<string>>> poem;

    for(size_t i=0; i<data.size(); i++){
        const char *cmd = translateDataToCommand(data.at(i), "p"); 
        poem.push_back(thpronunTextToSectionSyllable(cmd));
    }
    
    return poem;
}

int calculateRhyme(vector<string> sadab,
    vector<string> rub,
    vector<string> rong,
    vector<string> song,
    vector<string> lastSong){

    int score = 0;
    
    for(int i=0; i!=sadab.size(); ++i){
        for(int j=0; j!=rub.size(); ++j){
            for(int k=0; k!=rong.size(); ++k){
                for(int l=0; l!=song.size(); ++l){
                    if(sadab.size() > 0 && rub.size() > 0){
                        score += checkInterSectionRhyme(sadab, rub, 1) * 15;
                    } else if(rub.size() > 0 && rong.size() > 0){
                        score += checkInterSectionRhyme(rub, rong, 2) * 15;
                    } else if(rong.size() > 0 && song.size() > 0){
                        score += checkInterSectionRhyme(rong, song, 3) * 15;
                    }
                    if(sadab.size() > 0){
                        score += checkIntraSectionRhyme(sadab) * 3 +
                                 checkAlphabetRhyme(sadab) +
                                 checkTone(sadab, 1) * 3;
                    }
                    if(rub.size() > 0){
                        score += checkIntraSectionRhyme(rub) * 3 +
                                 checkAlphabetRhyme(rub) +
                                 checkTone(rub, 2) * 3;
                    }
                    if(rong.size() > 0){
                        score += checkIntraSectionRhyme(rong) * 3 +
                                 checkAlphabetRhyme(rong) +
                                 checkTone(rong, 3);
                    }
                    if(song.size() > 0){
                        score += checkIntraSectionRhyme(song) * 3 +
                                 checkAlphabetRhyme(song) +
                                 checkTone(song, 4);
                    }
                }
            }
        }
    }
    return score;
}

Result poemAnalyze(vector<vector<vector<string>>> poem){
    Result result;
    int score = 0;
    vector<int> bestPosition;
    vector<vector<string>> sadab;
    vector<vector<string>> rub;
    vector<vector<string>> rong;
    vector<vector<string>> song;
    vector<string> lastSong;

    for(int poemIndex=0; poemIndex!=poem.size(); poemIndex+=4){
        score = 0;
        for (int i=0; i<4; i++){
            if(poemIndex+i < poem.size()){
                break;
            }
            switch(i){
                case 0: sadab.assign(poem.at(poemIndex+i).begin(),poem.at(poemIndex+i).end());
                        break;
                case 1: rub.assign(poem.at(poemIndex+i).begin(),poem.at(poemIndex+i).end());
                        break;
                case 2: rong.assign(poem.at(poemIndex+i).begin(),poem.at(poemIndex+i).end());
                        break;
                case 3: song.assign(poem.at(poemIndex+i).begin(),poem.at(poemIndex+i).end());
                        break;
            }
        }

        for(int i=0; i!=sadab.size(); ++i){
            for(int j=0; j!=rub.size(); ++j){
                for(int k=0; k!=rong.size(); ++k){
                    for(int l=0; l!=song.size(); ++l){
                        if(score > calculateRhyme(sadab.at(i), rub.at(j), rong.at(k), song.at(l), lastSong)){
                            score = calculateRhyme(sadab.at(i), rub.at(j), rong.at(k), song.at(l), lastSong);
                            bestPosition.push_back(i);
                            bestPosition.push_back(j);
                            bestPosition.push_back(k);
                            bestPosition.push_back(l);
                        }
                    }
                }
            }
        }

        result.score = score;


    }

    return result;
}

Result poemAnalyze(vector<string> buffer){
    vector<vector<vector<string>>> poem;

    for(size_t i=0; i<buffer.size(); i++){
        const char *cmd = translateDataToCommand(buffer.at(i), "p"); 
        poem.push_back(thpronunTextToSectionSyllable(cmd));
    }
        
}

static PyObject *SpamError;

PyObject *vectorToList_String(vector<string> dataList){
    PyObject *pyList = PyList_New(dataList.size());
    if(!pyList)
        throw logic_error("unable to allocate memory for list");
    for(size_t i=0; i!=dataList.size(); i++){
        PyObject *data = PyUnicode_FromString(dataList.at(i).c_str());
        if(!data)
            throw logic_error("unable to allocate memmory for string");
        PyList_SetItem(pyList, i, data);
    }

    return pyList;
}

PyObject *vectorToList_Int(vector<int> dataList){
    PyObject *pyList = PyList_New(dataList.size());
    if(!pyList)
        throw logic_error("unable to allocate memory for list");
    for(size_t i=0; i!=dataList.size(); i++){
        PyObject *data = PyLong_FromLong(dataList.at(i));
        if(!data)
            throw logic_error("unable to allocate memmory for string");
        PyList_SetItem(pyList, i, data);
    }

    return pyList;
}

static PyObject *
alklorn(PyObject *self, PyObject *args){
    Result result;
    // PyObject *result;
    vector<string> get_input;
    string buffer;
    PyObject *obj;

    if(!PyArg_ParseTuple(args, "O", &obj))
        return NULL;
    
    if(!PyList_Check(obj))
        throw logic_error("not a list");
    for(Py_ssize_t i=0; i<PyList_Size(obj); i++){
        PyObject *iter = PyList_GetItem(obj, i);
        PyObject *str = PyUnicode_AsEncodedString(iter, "utf-8", "~E~");
        
        get_input.push_back( PyBytes_AS_STRING(str) );
    }

    result = call_alklorn(get_input);
    // cout << is_rhyme_in_alphabet("pheym2", "phahy0");

    // return Py_BuildValue("s","finished");
    return Py_BuildValue("{s : i, s : i, s : O, s : O, s : O, s : O, s : O, s:O}","score"
                                                                    ,result.score
                                                                    ,"suparb"
                                                                    ,result.suparb
                                                                    ,"klorn_result"
                                                                    ,vectorToList_String(result.klorn_result)
                                                                    ,"out_rhyme"
                                                                    ,vectorToList_Int(result.out_rhyme)
                                                                    ,"in_rhyme"
                                                                    ,vectorToList_Int(result.in_rhyme)
                                                                    ,"tone"
                                                                    ,vectorToList_Int(result.tone)
                                                                    ,"alpha_rhyme"
                                                                    ,vectorToList_Int(result.alpha_rhyme)
                                                                    ,"between_bot_rhyme"
                                                                    ,vectorToList_Int(result.between_bot_rhyme)
                                                                    );
}

static PyMethodDef alklornrithm_method[]={
    {"alklorn", alklorn, METH_VARARGS,
     "Execute a shell command."},
    {NULL, NULL , 0, NULL}//sentinel
};

static struct PyModuleDef alklornrithm_module{
    PyModuleDef_HEAD_INIT,
    "alklornrithm",//name of module
    NULL, // doc
    -1,
    alklornrithm_method
};

PyMODINIT_FUNC
PyInit_poem_analyze(void){
    return PyModule_Create(&alklornrithm_module);
}
