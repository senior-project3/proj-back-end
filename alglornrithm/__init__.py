__version__ = '0.1'

from flask import Flask
from flask_user import UserManager
from flask_cors import CORS

from . import views
from . import models


def init_login_manager(app):
    user_manager = UserManager(app, models.db, models.User)


def create_app():
    app = Flask(__name__)
    CORS(app)
    app.config.from_object('alglornrithm.default_settings')
    app.config.from_envvar('ALGLORNRITHM_SETTINGS', silent=True)

    init_login_manager(app)
    models.init_db(app)
    views.register_blueprint(app)

    return app
