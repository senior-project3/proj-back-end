import mongoengine as me
from flask_user import UserMixin
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from flask import current_app as app


class User(me.Document, UserMixin):
    meta = {'collection': 'users'}

    email_address = me.StringField(required=True, unique=True)
    username = me.StringField()
    password_hash = me.StringField()
    poems = me.ListField(me.ReferenceField('Poem', dbref=True))
    comments = me.ListField(me.ReferenceField('Poem', dbref=True))
    vocab = me.ListField(me.ReferenceField('Vocab', dbref=True))

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        id = str(self.id)
        return s.dumps({'id': id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user
