from flask import (Blueprint,
                   Flask,
                   render_template,
                   url_for,
                   redirect,
                   current_app,
                   session,
                   request,
                   json,
                   g)
from flask_user import (login_required,
                        current_user)
from alglornrithm import models
from flask_httpauth import HTTPBasicAuth


module = Blueprint('account', __name__, url_prefix='/account')
auth = HTTPBasicAuth()


@module.route('/', methods=['GET'])
def account():
    print('account')


@module.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    email_address = data['email']
    username = data['username']
    password = data['password']
    res = {
        'message': ""
    }
    if email_address is None or password is None:
        res['message'] = 'missing input'
        response = Flask.response_class(
            response=json.dumps(res),
            status=400,
            mimetype='application/json'
        )
        return response
    if models.User.objects(email_address=email_address).first():
        res['message'] = 'Duplicate User'
        response = Flask.response_class(
            response=json.dumps(res),
            status=200,
            mimetype='application/json'
        )
        return response
    user = models.User(email_address=email_address, username=username)
    user.hash_password(password)
    user.save()
    res['message'] = 'Register Success'
    response = Flask.response_class(
            response=json.dumps(res),
            status=200,
            mimetype='application/json'
        )
    return response


@module.route('/get_user/<email_address>')
@auth.login_required
def get_user(email_address):
    user = models.User.objects(email_address=email_address).first()
    if not user:
        response = Flask.response_class(
            response='User not Exist',
            status=200,
            mimetype='application/json'
        )
        return response
    user_data = {
        'id': str(user.id),
        'email': user.email_address,
        'username': user.username,
        'poems': user.poems,
        'comments': user.comments
    }
    response = Flask.response_class(
            response=json.dumps(user_data),
            status=200,
            mimetype='application/json'
        )
    return response


@module.route('/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token(600)
    response = Flask.response_class(
            response=json.dumps(token.decode('ascii')),
            status=200,
            mimetype='application/json'
        )
    return response


@auth.verify_password
def verify_password(email_address_or_token, password):
    print(email_address_or_token, password)
    # first try to authenticate by token
    user = models.User.verify_auth_token(email_address_or_token)
    if not user:
        # try to authenticate with username/password
        user = models.User.objects(email_address=email_address_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True


@module.route('/user/<user_id>')
def get_user_data(user_id):
    user = models.User.objects(id=user_id).first()
    if not user:
        response = Flask.response_class(
            response="",
            status=200,
            mimetype='application/json'
        )
        return response
    print(user)
    user_data = {
        'email': user.email_address,
        'username': user.username
    }
    response = Flask.response_class(
            response=json.dumps(user_data),
            status=200,
            mimetype='application/json'
        )
    return response

